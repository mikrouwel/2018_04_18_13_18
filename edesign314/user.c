
#include "userdefine.h"

char tempchar[3];
int counter = 0;
int temp = 0;
char buffrx;
char input[10];

volatile int rxflag = 0;
volatile int tick;


char studentnumber[13]={'$', 'A', ',' , '1','9','0','8','3','9','6','3', 0x0D, 0x0A} ;
char SetTempReply[4] = {'$','F', 0x0D, 0x0A};



void userProcessInit(void)
{
	HAL_UART_Receive_IT(&huart1, (uint8_t*)&buffrx, 1);
	SysTick_Config(SystemCoreClock/1000);
	tick = 0;

	LEDInit();
}

void userProcess(void)
{

	 HAL_UART_Receive_IT(&huart1, (uint8_t*)&buffrx, 1);
	 if (rxflag == 1 && input[0] == '$' && input[1] == 'A')
			  {
				  	 HAL_UART_Transmit(&huart1, (uint8_t*)&studentnumber, 13, 1000);
				  	 //HAL_UART_Transmit_IT(&huart1, (uint8_t*)&input, counter);
				  	 rxflag = 0;
			  }

			  if (rxflag == 1 && input[0] == '$' && input[1] == 'F' && input[2] == ',')
			  {
				  	  tempchar[0] = input[3];
				  	  tempchar[1] = input[4];
				  	  tempchar[2] = input[5];
				  	  temp = atoi(tempchar);

				  	  HAL_UART_Transmit(&huart1, (uint8_t*)&SetTempReply, 4, 1000);
				  	  rxflag = 0;
			  }

			  if (rxflag == 1 && input[0] == '$' && input[1] == 'G' && temp>-1 && temp<101)
			  {
				  char tempcharfinal[8] = {'0','0','0','0','0','0',0x0D,0x0A};

				  if(temp == 100)
				  {
					  char tempcharreturn[8] = {'$','G',',','1','0','0',0x0D,0x0A};
					  for (int i = 0; i<8; i++){
						  tempcharfinal[i] = tempcharreturn[i];
					  }

					  HAL_UART_Transmit(&huart1, (uint8_t*)&tempcharfinal, 8, 1000);
				  }

				  if(temp != 100)
				  {
					  char tempcharreturn[8] = {'$','G',',',tempchar[0],tempchar[1], 0x0D, 0x0A};
					  for (int i = 0; i<8; i++){
						  tempcharfinal[i] = tempcharreturn[i];
					  }
					  HAL_UART_Transmit(&huart1, (uint8_t*)&tempcharfinal, 7, 1000);
				  }


			  		rxflag = 0;
			  }



			  if(tick == 5)
			  {

				  DisplayTemp(temp);
				  tick = 0;
			  }


}

void DisplayTemp(int temp)
{
	switch (temp){

	case 0:
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,0);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,0);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,1);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,0);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,0);

	break;
	case 1:
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,1);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,1);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,1);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,1);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,1);
	break;
	}


}

void LEDInit(void)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_SET);
}

void HAL_SysTick_Callback()
{

	tick++;

}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	if(huart->Instance == USART1)
	{

	 if (buffrx != 0x0A)
	 {
		 input[counter] = buffrx;
		 counter++;
	 }
	 else
	 {
		rxflag = 1;
		counter = 0;
	 }

	 HAL_UART_Receive_IT(&huart1, (uint8_t *)&buffrx, 1);
	}


}
