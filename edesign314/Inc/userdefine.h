#ifndef USER_H_
#define USER_H_

#include <stdbool.h>
#include "main.h"
#include "stm32f3xx_hal.h"
#include <math.h>
#include <stdint.h>
#include <float.h>
#include <string.h>
#include <stdlib.h>

extern UART_HandleTypeDef huart1;
extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_adc1;

void userProcessInit(void);

void userProcess(void);

void LEDInit(void);

#endif
