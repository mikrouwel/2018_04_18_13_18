
#include "userdefine.h"


char tempchar[3];
int counter = 0;
int temp = 0;
int scaledTemp;
char buffrx;
char input[10];
int total_flow = 0; // ml
char valve = 0;
char heat = 0;

uint32_t ADCValI[501];
uint32_t ADCValV[501];
uint32_t Temp_sensor = 0;
uint32_t Temp = 0;
uint32_t buffADC[4];

/*
uint32_t ADCISum = 0;
uint32_t ADCVSum = 0;

uint32_t ADCIMean = 0;
uint32_t ADCVMean = 0;

uint32_t ADCISQSUM = 0;
uint32_t ADCVSQSUM = 0;

uint32_t ADCIRMS = 0;
uint32_t ADCVRMS = 0;
*/

int ADCISum = 0;
int ADCVSum = 0;

int ADCIMean = 0;
int ADCVMean = 0;

int ADCISQSUM = 0;
int ADCVSQSUM = 0;

int ADCIRMS = 0;
int ADCVRMS = 0;

int scaledIRMS = 0;
int scaledVRMS = 0;

int ADCIRMSTH = 0;
int ADCVRMSTH = 0;

int ADCIMeanTH = 0;
int ADCVMeanTH = 0;

int maxI = 0;
int maxV = 0;

volatile int rxflag = 0;
volatile int tick = 0;
volatile int ADCtick = 0;
volatile int ADCFlag = 0;
volatile int dmacmplt = 0;

char studentnumber[13]={'$', 'A', ',' , '1','9','0','8','3','9','6','3', 0x0D, 0x0A} ;
char SetTempReply[4] = {'$','F', 0x0D, 0x0A};
char SetValve[4] = {'$','B', 0x0D, 0x0A};
char SetHeater[4] = {'$','D', 0x0D, 0x0A};

void userProcessInit(void)
{
	HAL_UART_Receive_IT(&huart1, (uint8_t*)&buffrx, 1);
	SysTick_Config(SystemCoreClock/1000);
	tick = 0;
	ADCtick = 0;

	LEDInit();
}

void DisplayTempDigit(int scaledTemp)
{
	//HAL_UART_Transmit(&huart1, (uint8_t*)&studentnumber, 13, 1000);

	switch (scaledTemp){

	case 0:
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,1);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,0);

	break;
	case 1:
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,1);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,1);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,1);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,1);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,1);
	break;
	case 2:
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,1);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,1);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,0);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,0);
		break;
	case 3:
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,1);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,0);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,1);
		break;
	case 4:
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,1);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,0);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,1);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,1);
		break;
	case 5:
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,1);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,0);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,1);
		break;
	case 6:
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,1);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,0);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,0);
		break;
	case 7:
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,1);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,1);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,1);
		break;
	case 8:
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,0);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,0);
		break;
	case 9:
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8,0);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9,0);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6,0);
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7,1);
		break;
	}


}

void DisplayTempUnit()
{
	char leadzeroh = 1;
	char leadzerot = 1;
	int hunds = temp;
	int tens = hunds % 100; // remainder after dividing by 100
	int units = tens % 10; // remainder after dividing by 10

	if (hunds/100 == 0)
	{
		leadzeroh = 0;
		if (tens/10 == 0)
		{
			leadzerot = 0;
		}
	}

	if (tick == 5 && leadzeroh)
//		if (tick == 5 && (hunds/100) > 0)
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3,GPIO_PIN_SET);
		DisplayTempDigit(hunds/100);
	}

	if (tick == 10 && leadzerot)
//		if (tick == 10 && (tens/10) > 0)
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3,GPIO_PIN_SET);
		DisplayTempDigit(tens/10);
	}

	if (tick == 15)
//		if (tick == 15 && (units) > 0)
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3,GPIO_PIN_RESET);
		DisplayTempDigit(units);
	}

	if (tick > 15)
	{
		tick = 0;
	}
}

void LEDInit(void)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_SET);
}



int findMax(int a[]) {
	int max;

	max = a[0];

	for (int i = 1; i < 201; i++) {
		if (a[i] > max) {

			max = a[i];

		}

	}
	return max;
}

void SampleADC() {
//	if (ADCFlag == 1) {


//		HAL_ADC_Start(&hadc1);
		HAL_ADC_Start_DMA(&hadc1, buffADC, 4);
//		while (dmacmplt == 1) {
//
//		}
		dmacmplt = 0;


		ADCValI[ADCtick] = buffADC[0];
		ADCValV[ADCtick] = buffADC[1];
		Temp = buffADC[2];
		Temp_sensor = buffADC[3];


//		HAL_ADC_Stop_DMA(&hadc1);

		if (ADCtick == 500) {
			ADCISum = 0;
			ADCISQSUM = 0;
			ADCVSum = 0;
			ADCVSQSUM = 0;
			for (int i = 1; i < 501; i++) {
				ADCISum += ADCValI[i];
				ADCVSum += ADCValV[i];
			}

			ADCIMean = ADCISum / 500;
			ADCVMean = ADCVSum / 500;

			for (int i = 1; i < 501; i++) {
				ADCISQSUM += (ADCValI[i] - ADCIMean) * (ADCValI[i] - ADCIMean);
				ADCVSQSUM += (ADCValV[i] - ADCIMean) * (ADCValV[i] - ADCIMean);

			}

			ADCIRMS = sqrt(ADCISQSUM / 500);
			ADCVRMS = sqrt(ADCVSQSUM / 500);

			ADCIRMSTH = ADCIRMS*1000;
			ADCVRMSTH = ADCVRMS*1000;

			ADCIMeanTH = ADCIMean*1000;
			ADCVMeanTH = ADCIMean*1000;

			maxI = findMax(ADCValI);
			maxV = findMax(ADCValV);

			scaledIRMS = ADCIRMSTH/(maxI - ADCIMean);
			scaledVRMS = ADCVRMSTH/(maxV - ADCVMean);

			//printf("%u", ADCIRMS);
			//printf("%u", ADCVRMS);

			ADCtick = 0;
		}

//		ADCFlag = 0;
//	}

}



void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	dmacmplt = 1;
	HAL_ADC_Stop_DMA(hadc);

	return;
}

void userProcess(void)
{
	static char valve_state[7];
	static char heat_state[4];

	memset(valve_state, '\0', 7);
	memset(heat_state, '\0', 4);

	if (valve == 1) {
		sprintf(valve_state, "OPEN");
	} else {
		sprintf(valve_state, "CLOSED");
	}

	if (heat == 1) {
		sprintf(heat_state, "ON");
	} else {
		sprintf(heat_state, "OFF");
	}

	//ADCtick = 0;
	SampleADC();

	HAL_UART_Receive_IT(&huart1, (uint8_t*) &buffrx, 1);

	if (rxflag == 1 && input[0] == '$' && input[1] == 'A') {
		HAL_UART_Transmit(&huart1, (uint8_t*) &studentnumber, 13, 1000);
		//HAL_UART_Transmit_IT(&huart1, (uint8_t*)&input, counter);
		rxflag = 0;
	}

	if (rxflag == 1 && input[0] == '$' && input[1] == 'B') {
//		valve = input[3] - '0';
		if (input[3] == '0')
		{
			valve = 0;
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10,0);
//			valve_state = 'CLOSED';
		}
		else
		{
			valve = 1;
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10,1);
//			valve_state = 'OPEN';
		}
		HAL_UART_Transmit(&huart1, (uint8_t*) &SetValve, 4, 1000);
			rxflag = 0;
	}

	if (rxflag == 1 && input[0] == '$' && input[1] == 'D') {
		heat = input[3] - '0';
		if (input[3] == '0')
		{
			heat = 0;
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, 0);
//			HAL_UART_Transmit(&huart1, (uint8_t*) &studentnumber, 13, 1000);
		}
		else
		{
			heat = 1;
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, 1);
		}


		HAL_UART_Transmit(&huart1, (uint8_t*) &SetHeater, 4, 1000);
			rxflag = 0;
	}

	if (rxflag == 1 && input[0] == '$' && input[1] == 'K') {

		//int Temp_sensor_degrees = (Temp_sensor-409.6)/8.192;
		int Temp_degrees = (Temp-1088)/12.4;
		int Temp_sensor_degrees = (Temp_sensor-1088)/12.4;

		//char[] decIRMS = ADCIRMS.ToString().ToCharArray();
		//char[] decVRMS = ADCVRMS.ToString().ToCharArray();
		char AR[40] = "";
		int ratioI = ((maxI-ADCIMean)*1000)/1583;
		int ratioV = ((maxV-ADCVMean)*1000)/1528;
		int finalI = ((scaledIRMS*13*ratioI)/1000);
		int finalV = ((scaledVRMS*220*ratioV)/1000);

		memset(AR, '\0', 40);
		sprintf(AR,"$K,%d,%d,%d,%d,%d,%s,%s\r\n",finalI,finalV,Temp_sensor_degrees,Temp_degrees,total_flow,heat_state,valve_state);

		HAL_UART_Transmit(&huart1, (uint8_t*)&AR, strlen(AR), 1000);

		rxflag = 0;
	}

	if (rxflag == 1 && input[0] == '$' && input[1] == 'F' && input[2] == ',') {
		tempchar[0] = input[3];
		tempchar[1] = input[4];
		tempchar[2] = input[5];
		temp = atoi(tempchar);

		HAL_UART_Transmit(&huart1, (uint8_t*) &SetTempReply, 4, 1000);
		rxflag = 0;
	}

	if (rxflag == 1 && input[0] == '$' && input[1] == 'G' && temp > -1
			&& temp < 101) {
		char tempcharfinal[8] = { '0', '0', '0', '0', '0', '0', 0x0D, 0x0A };

		if (temp == 100) {
			char tempcharreturn[8] =
					{ '$', 'G', ',', '1', '0', '0', 0x0D, 0x0A };
			for (int i = 0; i < 8; i++) {
				tempcharfinal[i] = tempcharreturn[i];
			}

			HAL_UART_Transmit(&huart1, (uint8_t*) &tempcharfinal, 8, 1000);
		}

		if (temp != 100) {
			char tempcharreturn[8] = { '$', 'G', ',', tempchar[0], tempchar[1],
					0x0D, 0x0A };
			for (int i = 0; i < 8; i++) {
				tempcharfinal[i] = tempcharreturn[i];
			}
			HAL_UART_Transmit(&huart1, (uint8_t*) &tempcharfinal, 7, 1000);
		}

		rxflag = 0;
	}

	DisplayTempUnit();


}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {


	static uint32_t  switch_time = 0;
	uint32_t  current_t;

	if (FlowPin_Pin == 1) {
		HAL_UART_Transmit(&huart1, (uint8_t*) &studentnumber, 13, 1000);


		current_t = HAL_GetTick();
		if ((current_t - switch_time) > 5)
		{

			switch_time = current_t;
			total_flow += 100;

		}

	}
	/*
	if (GPIO_Pin == FlowPin_Pin) {

		startTick = flowCounter;

	}
	else
	{
		if((flowCounter - startTick) > 5)
		{
			flowCounter = 0;
			total_flow += 100;
		}
	}
*/
}



void HAL_SYSTICK_Callback(void)
{
	//HAL_UART_Transmit(&huart1, (uint8_t*)&studentnumber, 13, 1000);
	tick++;
	ADCtick++;

	ADCFlag = 1;
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	//if(huart->Instance == USART1)
	//{

	 if (buffrx != 0x0A)
	 {
		 input[counter] = buffrx;
		 counter++;
	 }
	 else
	 {
		rxflag = 1;
		counter = 0;
	 }

	 //HAL_UART_Receive_IT(&huart1, (uint8_t *)&buffrx, 1);
	//}

}
